Parametric Hypotrochoid Gear Set
================================

A 3D printable set of gears to draw [Hypotrochoids](https://en.wikipedia.org/wiki/Hypotrochoid),
similar to one of the usages of [Spirograph](https://en.wikipedia.org/wiki/Spirograph),

The OBJ files are printable as provided. Choose slicer settings for
high quality; artifacts from roughness on the teeth will make the
set work poorly and/or be visible in the drawings.

To use, flip the rings over, so that the flange is above the teeth.
The flange helps hold the teeth of the gears down so they don't jump
out.

The gears are used as printed. The upper tips of the gears are
chamfered to keep them from catching on the flange.

If you want to make other size rings and gears, the FCStd files were
created with [FreeCAD](https://www.freecad.org/) 0.20.2 and the
[FCGear workbench](https://wiki.freecadweb.org/FCGear_Workbench).
You will need the FCGear Workbench installed to make modifications.

In FreeCAD, select the gray **InvoluteGear** in the **Gear**, or the
gray **InternalInvoluteGear** in the **Ring**, and then change the
**base** property **teeth** to the number of teeth you want. (If
you want a **Gear** with fewer than 15 teeth, you might need to
edit the **Drawing Holes Pocket** sketch and delete some holes
near the center, and increase the spacing between the holes.)
Then select the **Body** and **File → Export** to save.

If you would like larger teeth (and, naturally resulting size),
change the **base** property **module** from 2mm to 3mm for both
the **Ring** and the **Gear** and regenerate all the rings and
gears you wish to use.

The Assembly.FCStd file will let you see whether your chosen Gear
and Ring parameters will result in a working system. If the gear
teeth overlap in the Assembly, then it will not work as printed.
After changing the tooth parameters in the Gear and Ring, you
will need to Recompute the model in the Assembly to see the
effect of your changes. It requires the Assembly4 workbench to
be installed.

Print SizeTemplate.pdf without scaling to easily identify rings
and gears by matching them against the printout. Additionally,
the gears and rings have their tooth count embossed/debossed on
them for easy identification.


Credit
======

Inspired by https://www.myminifactory.com/object/3d-print-spirograph-11148
but that has only STLs and no ability to make different objects.
I have [reverse engineered a probably compatible tooth
form](https://forum.makerforums.info/t/reverse-engineering-a-3d-printing-gear-model/86472?u=mcdanlj)
and you could use those parameters with this design, to make additional
rings or gears with that system. You'll need to adjust heights as well;
I made this set thicker.


License
=======

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
